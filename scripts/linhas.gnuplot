# Inicialização
clear
reset
set key off
set encoding iso_8859_15				# Codifição

# Configurações de saida
# Inclui formato de exportação, tamanho do gráfico, fontes utilizadas
# e nome do arquivo de saída

# Exportação para o formato .png
# set terminal png size 640,480 enhanced font 'Helvetica,12'
# set output 'output/linhas.png'

# Exportação para o formato .jpg
#set terminal jpeg size 640,480 enhanced font 'Helvetica' 12
#set output 'output/linhas.jpg'

# Exportação para o formato .svg
set terminal svg size 640,480 enhanced background rgb 'white' fname 'Helvetica' fsize 14 butt solid
set output 'output/linhas.svg'

# Título do gráfico
set title 'Taxa de crescimento de nascidos vivos no RN (1994-2014)'

# Configurações do eixo horizontal
set xtic rotate by -90 scale 0 offset -1, -0.25		# Rotacao dos rótulos no eixo

# Configurações do eixo vertical
set ytics 40										# Salto entre valores
set ylabel 'Taxa de crescimento (%)'				# Rótulo do eixo

# Configuração do tipo de gráfico a ser gerado (gráfico de linhas)
set key inside top right		# Posicionamento da legenda (superior direito)
set style data lines

# Plotagem do gráfico
# Os dados a serem plotados constam no arquivo extra.dat
plot for [i=2:6] 'output/extra.dat' using i:xticlabels(1) title columnheader(i)
