# Makefile - Laboratorio 4
#
# Makefile completo separando os diferentes elementos da aplicacao 
# como codigo (src), cabecalhos (include), executaveis (build),
# bibliotecas (lib), etc. Cada elemento e alocado em uma pasta especifica, organizando melhor o codigo fonte.
#
# Algumas variaveis sao usadas com significado especial:
#
# $@ nome do alvo (target)
# $^ lista com os nomes de todos os pre-requisitos sem duplicatas
# $< nome do primeiro pre-requisito
#

# Comandos do sistema operacional
# Linux: rm -rf 
# Windows: cmd //C del 
RM = rm -rf 

# Compilador
CC=g++ 

# Variaveis para os subdiretorios
LIB_DIR=lib
INC_DIR=include
SRC_DIR=src
OBJ_DIR=build
BIN_DIR=bin
DOC_DIR=doc
OUT_DIR=output
 
# Opcoes de compilacao
CFLAGS=-Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

# Garante que os alvos desta lista nao sejam confundidos com arquivos 
# de mesmo nome
.PHONY: all init clean debug doxy doc

# Define o alvo (target) para a compilacao completa e os alvos de 
# dependencia. Ao final da compilacao, remove os arquivos objeto.
all: init nascimentos extra

debug: CFLAGS += -g -O0
debug: all

# Alvo (target) para a criação da estrutura de diretorios
# necessaria para a geracao dos arquivos objeto 
init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/
	@mkdir -p $(OUT_DIR)/

# Alvo (target) para a construcao do executavel nascimentos
# Define os arquivos build/arquivos.o e build/main.o como dependencias
nascimentos: $(OBJ_DIR)/arquivos.o $(OBJ_DIR)/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/nascimentos $^
	@echo "+++ [Executavel nascimentos criado em $(BIN_DIR)] +++"
	@echo "============="

# Alvo (target) para a construcao do objeto build/arquivos.o
# Define os arquivos src/arquivo.cpp, include/stats.h e include/arquivos.h como
# dependencias
$(OBJ_DIR)/arquivos.o: $(SRC_DIR)/arquivos.cpp $(INC_DIR)/stats.h $(INC_DIR)/arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto build/main.o
# Define os arquivos src/main.cpp, include/stats.h e include/arquivos.h como
# dependencias
$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp $(INC_DIR)/stats.h $(INC_DIR)/arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do executavel extra
# Define os arquivos build/arquivos.o e build/extra.o como dependencias
extra: $(OBJ_DIR)/arquivos.o $(OBJ_DIR)/extra.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/extra $^
	@echo "+++ [Executavel nascimentos criado em $(BIN_DIR)] +++"
	@echo "============="

# Alvo (target) para a construcao do objeto build/extra.o
# Define os arquivos src/extra.cpp, include/stats.h e include/arquivos.h como
# dependencias
$(OBJ_DIR)/extra.o: $(SRC_DIR)/extra.cpp $(INC_DIR)/stats.h $(INC_DIR)/arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a geração automatica de documentacao usando o 
# Doxygen. Sempre remove a documentacao anterior (caso exista) e 
# gera uma nova.
doxy:
	doxygen -g

doc:
	$(RM) $(DOC_DIR)/*
	doxygen

# Alvo (target) usado para limpar os arquivos temporarios (objeto)
# gerados durante a compilacao, assim como os arquivos 
# binarios/executaveis.
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
