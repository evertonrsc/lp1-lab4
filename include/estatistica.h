/**
 * @file	estatistica.h
 * @brief	Implementacao de funcoes para computacao de estatisticas 
 *			sobre uma amostra de dados
 * @details	As estatisticas computadas sao maximo, minimo, media, 
 *			desvio padrao e total na amostra em questao
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	06/04/2017
 * @date	24/04/2017
 */

#ifndef ESTATISTICA_H
#define ESTATISTICA_H

#include <cmath>
using std::pow;
using std::sqrt;


/**
 * @brief Funcao generica que retorna o maior valor em uma amostra
 * @param amostra Amostra de dados em questao
 * @param tamanho Tamanho da amostra
 * @return Maior valor na amostra
 */
template<typename T>
T maximo(T* amostra, int tamanho) {
	T max = amostra[0];
	for (int i = 1; i < tamanho; i++) {
		if (amostra[i] > max) {
			max = amostra[i];
		}
	}
	return max;
}


/**
 * @brief Funcao generica que retorna o menor valor em uma amostra
 * @param amostra Amostra de dados em questao
 * @param tamanho Tamanho da amostra
 * @return Menor valor na amostra
 */
template<typename T>
T minimo(T* amostra, int tamanho) {
	T min = amostra[0];
	for (int i = 1; i < tamanho; i++) {
		if (amostra[i] < min) {
			min = amostra[i];
		}
	}
	return min;
}


/**
 * @brief Funcao generica que retorna a soma dos valores em uma amostra
 * @param amostra Amostra de dados em questao
 * @param tamanho Tamanho da amostra
 * @return Soma dos valores da amostra
 */
template<typename T>
T total(T* amostra, int tamanho) {
	T soma = (T) 0;
	for (int i = 0; i < tamanho; i++) {
		soma += amostra[i];
	}
	return soma;
}


/**
 * @brief Funcao generica que retorna o valor medio de uma amostra
 * @param amostra Amostra de dados em questao
 * @param tamanho Tamanho da amostra
 * @return Valor medio da amostra
 */
template<typename T>
float media(T* amostra, int tamanho) {
	return (float) total(amostra, tamanho) / tamanho;
}


/**
 * @brief Funcao generica que retorna o desvio padrao em uma amostra
 * @param amostra Amostra de dados em questao
 * @param tamanho Tamanho da amostra
 * @return Desvio padrao na amostra
 */
template<typename T>
float desvpad(T* amostra, int tamanho) {
	T med = (T) media(amostra, tamanho);
	float desvio = 0.0;
	for (int i = 0; i < tamanho; i++) {
		desvio += pow(amostra[i] - med, 2);
	}
	return sqrt(desvio / tamanho);
}


/**
 * @brief Funcao generica que retorna a razao entre dois dados numericos
 * @param numerador Numerador
 * @param denominador Denominador
 * @return Razao
 */
template<typename T>
float razao(T numerador, T denominador) {
	return (float) numerador / denominador;
}

#endif
