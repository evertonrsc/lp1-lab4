/**
 * @file	arquivos.h
 * @brief	Definicao de funcoes auxiliares para a manipulacao de
 *			arquivos
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	06/04/2017
 * @date	28/04/2017
 */

#ifndef ARQUIVOS_H
#define ARQUIVOS_H

#include <fstream>
using std::ifstream;

#include "stats.h"


/**
 * @brief 	Funcao generica que determina se o stream de arquivo e invalido
 * @details Caso o stream seja invalido, uma mensagem de erro e exibida e o programa
 *			encerrado
 * @param	arquivo Stream de arquivo (entrada ou saida)
 * @param	nome Nome do arquivo em questao
 */
template<typename stream>
void verificaArquivo(const stream& arquivo, string nome) {
	if (arquivo.bad() || !arquivo || (arquivo.is_open() == 0)) {
		cerr << "Arquivo " << nome << " nao pode ser aberto." << endl;
		cerr << "O programa sera encerrado." << endl;
		exit(1);
	}
}


/**
 * @brief Realiza leitura de dataset
 * @param dataset Stream de entrada de arquivo referente ao dataset completo
 * @param municipios Numero de municipios
 * @return Vetor de objetos do tipo estrutura Stats para armazenar as estatisticas 
 *		   de cada municipio
 */
Stats* carregarDataset(ifstream& dataset, int municipios);


/**
 * @brief 	Computa as estatisticas para cada ano
 * @details Gera dois arquivos de saida, um contendo as estatisticas de
 *			nascimentos para cada ano e outro contendo apenas o total de
 *			nascimentos para cada ano
 * @param 	estatisticas Vetor de objetos do tipo estrutura Stats 
 *		  	para armazenar as estatisticas de cada municipio
 * @param 	municipios Numero de municipios
 */
void gerarEstatisticas(Stats* estatisticas, int municipios);


/**
 * @brief 	Calcula a taxa de crescimento percentual para um conjunto de municipios 
 *			na serie historica
 * @param 	Vetor de objetos do tipo estrutura Stats para armazenar as estatisticas 
 *		  	de cada municipio
 * @param 	municipios Numero de municipios
 * @return 	Matriz contendo a taxa de crescimento de cada municipio na serie historica
 */
float** calcularCrescimentoSerie(Stats* estatisticas, int municipios);


/**
 * @brief 	Realiza uma avaliacao comparativa do numero de nascimentos em dois 
 *			anos especificos
 * @details Faz uso da taxa de crescimento entre os dois anos em questao para 
 *			identificar que municipios apresentaram maior e menor valores
 * @param 	estatisticas Vetor de objetos do tipo estrutura Stats 
 *		  	para armazenar as estatisticas de cada municipio
 * @param 	municipios Numero de municipios
 * @param	ano1 Ano base de comparacao
 * @param	ano2 Ano base de comparacao
 */
void avaliarCrescimento(Stats* estatisticas, int municipios, int ano1, int ano2);


/**
 * @brief 	Realiza leitura de arquivo com municipios alvo
 * @param 	estatisticas Vetor de objetos do tipo de estrutura Stats para armazenar
 *		  	as estatisticas do dataset completo
 * @param 	municipios Numero total de municipios
 * @param 	arq_alvos Stream de entrada de arquivo referente aos municipios alvo
 * @param	numAlvos Numero de municipios alvo
 * @return 	Vetor de objetos do tipo estrutura Stats para armazenar as estatisticas 
 *		   	de cada municipio
 */
Stats* carregarAlvos(Stats* estatisticas, int municipios, ifstream& arq_alvos, int numAlvos);


/**
 * @brief 	Calcula as taxas de crescimento para um conjunto de municipios para a
 *		  	serie considerada
 * @details	Gera um arquivo de saida contendo as taxas de crescimento de cada municipio
 *			na serie historica
 * @param 	estatisticas Vetor de objetos do tipo estrutura Stats para armazenar as 
 *		  	estatisticas dos municipios em questao
 * @param 	municipios Numero de municipios
 */
void analisarCrescimentoSerie(Stats* estatisticas, int municipios);

#endif
