/**
 * @file	stats.h
 * @brief	Definicao da estrutura Stats, que agrega os dados referentes
 *			aos nascimentos em um municipio
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	06/04/2017
 * @date	24/04/2017
 */

#ifndef STATS_H
#define STATS_H

#include <string>
using std::string;


/** 
 * @struct  Stats stats.h
 * @brief	Tipo estrutura que agrega os dados referentes aos 
 *			nascimentos em um municipio
 * @details Os campos deste tipo estrutura sao o codigo e o nome do
 *			municipio, bem como o numero de nascidos vivos entre os
 *			anos de 1994 e 2014
 */
struct Stats {
	string codigo;			/**< Codigo do municipio */
	string nome;			/**< Nome do municipio   */
	int nascimentos[21];	/**< Numero de nascimentos em cada ano */
};

#endif
