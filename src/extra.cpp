/**
 * @file	extra.cpp
 * @brief	Codigo fonte principal de programa extra
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	06/04/2017
 * @date	28/04/2017
 */

#include <cstdlib>
using std::exit;

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include <string>
using std::string;

#include "arquivos.h"
#include "stats.h"


/** @brief Funcao principal */
int main(int argc, char* argv[]) {
	if (argc != 2) {
		cerr << "Execucao incorreta do programa" << endl;
		cerr << "Modo de uso: ./nascimentos <arquivo_entrada>" << endl;
		exit(1);
	} else {
		// Stream de entrada de arquivo
		ifstream dataset(argv[1]);
		verificaArquivo(dataset, argv[1]);

		// Leitura previa para identificar a quantidade de municipios
		int municipios = 0;
		string buffer;
		while (getline(dataset, buffer)) {
			municipios++;
		}
		municipios = municipios-2;

		// Vetor de objetos do tipo estrutura Stats para representar as
		// estatisticas dos municipios
		Stats* estatisticas = carregarDataset(dataset, municipios);
		dataset.close();

		// Computacao das estatisticas e geracao dos arquivos de saida
		gerarEstatisticas(estatisticas, municipios);

		// Stream de entrada de arquivo
		ifstream arq_alvos("input/alvos.dat");
		verificaArquivo(arq_alvos, "input/alvos.dat");

		// Leitura previa para identificar a quantidade de municipios
		int numAlvos = 0;
		buffer.clear();
		while (getline(arq_alvos, buffer)) {
			numAlvos++;
		}

		// Vetor de objetos do tipo estrutura Stats para representar as
		// estatisticas dos municipios alvo
		Stats* alvos = carregarAlvos(estatisticas, municipios, arq_alvos, numAlvos);
		arq_alvos.close();

		// Analise de serie historica para o conjunto de municipios alvo
		analisarCrescimentoSerie(alvos, numAlvos);
		
		// Liberacao de memoria dinamicamente alocada
		if (estatisticas != NULL) {
			delete[] estatisticas;
		}
		if (alvos != NULL) {
			delete[] alvos;
		}
	}

	return 0;
}
