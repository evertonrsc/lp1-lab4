/**
 * @file	arquivos.cpp
 * @brief	Implementacao de funcoes auxiliares para a manipulacao de
 *			arquivos
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	06/04/2017
 * @date	28/04/2017
 * @sa		arquivos.h
 */

#include <climits>

#include <cstdlib>
using std::exit;

#include <fstream>
using std::fstream;
using std::ifstream;
using std::ofstream;

#include <iomanip>
using std::fixed;
using std::setprecision;

#include <ios>
using std::showpos;
using std::noshowpos;

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include <string>
using std::string;

#include "arquivos.h"
#include "estatistica.h"

#define SERIE 21			/**< Numero de anos considerado na serie */


/**
 * @brief Realiza leitura de dataset
 * @param dataset Stream de entrada de arquivo referente ao dataset completo
 * @param municipios Numero de municipios
 * @return Vetor de objetos do tipo estrutura Stats para armazenar as estatisticas 
 *		   de cada municipio
 */
Stats* carregarDataset(ifstream& dataset, int municipios) {
	Stats* estatisticas = new Stats[municipios];
	if (!estatisticas) {
		cerr << "Erro em alocacao dinamica de memoria." << endl;
		cerr << "O programa sera encerrado." << endl;
		exit(1);
	}

	string buffer;
	dataset.clear();
	dataset.seekg(0, dataset.beg);		// Leitura a partir do inicio
	getline(dataset, buffer);			// Ignora primeira linha
	
	for (int i = 0; i < municipios; i++) {
		// Codigo do municipio
		getline(dataset, buffer, ' ');
		estatisticas[i].codigo = buffer.substr(1, buffer.length()-1);

		// Nome do municipio
		getline(dataset, buffer, ';');
		estatisticas[i].nome = buffer.substr(0, buffer.length()-1);

		// Nascimentos na serie historica
		for (int j = 0; j < SERIE; j++) {
			getline(dataset, buffer, ';');			
			estatisticas[i].nascimentos[j] = atoi(buffer.c_str());
		}
		dataset.ignore(INT_MAX, '\n');		// Ignora restante da linha
	}

	return estatisticas;
}


/**
 * @brief 	Computa as estatisticas para cada ano
 * @details Gera dois arquivos de saida, um contendo as estatisticas de
 *			nascimentos para cada ano e outro contendo apenas o total de
 *			nascimentos para cada ano
 * @param 	estatisticas Vetor de objetos do tipo estrutura Stats 
 *		  	para armazenar as estatisticas de cada municipio
 * @param 	municipios Numero de municipios
 */
void gerarEstatisticas(Stats* estatisticas, int municipios) {
	// Stream de saida em arquivo para estatisticas
	ofstream arq_estatisticas("output/estatisticas.csv");
	verificaArquivo(arq_estatisticas, "output/estatisticas.csv");

	// Stream de saida em arquivo para totais
	ofstream arq_totais("output/totais.dat");
	verificaArquivo(arq_totais, "output/totais.dat");

	// Contabilizacao de nascimentos em cada ano
	for (int i = 0; i < SERIE; i++) {
		int* nascimentos_ano = new int[municipios];
		if (!nascimentos_ano) {
			cerr << "Erro em alocacao dinamica de memoria." << endl;
			cerr << "O programa sera encerrado." << endl;
			exit(1);
		}
		for (int j = 0; j < municipios; j++) {
			nascimentos_ano[j] = estatisticas[j].nascimentos[i];
		}

		// Computacao das estatisticas
		int max = maximo(nascimentos_ano, municipios);		// Maior valor
		int min = minimo(nascimentos_ano, municipios);		// Menor valor
		float med = media(nascimentos_ano, municipios);		// Valor medio
		float desv = desvpad(nascimentos_ano, municipios);	// Desvio padrao
		int soma = total(nascimentos_ano, municipios);		// Total

		// Gravacao das estatisticas em arquivo de saida
		arq_estatisticas << (1994 + i) << ';';
		arq_estatisticas << max << ';' << min << ';' << med << ';';
		arq_estatisticas << desv << ';' << soma << endl;

		// Gravacao dos totais em arquivo de saida
		arq_totais << (1994 + i) << ' ' << soma << endl;

		// Liberacao de memoria dinamicamente alocada
		if (nascimentos_ano != NULL) {
			delete[] nascimentos_ano;
		}
	}

	cout << "... Arquivo output/estatisticas.csv gerado" << endl;
	arq_estatisticas.close();

	cout << "... Arquivo output/totais.dat gerado" << endl;
	arq_totais.close();
}


/**
 * @brief 	Calcula a taxa de crescimento percentual para um conjunto de municipios 
 *			na serie historica
 * @param 	Vetor de objetos do tipo estrutura Stats para armazenar as estatisticas 
 *		  	de cada municipio
 * @param 	municipios Numero de municipios
 * @return 	Matriz contendo a taxa de crescimento de cada municipio na serie historica
 */
float** calcularCrescimentoSerie(Stats* estatisticas, int municipios) {
	float** taxasCrescimento = new float*[municipios];
	for (int i = 0; i < municipios; i++) {
		taxasCrescimento[i] = new float[SERIE-1];
	}

	// Calculo da taxa de crescimento percentual
	for (int i = 0; i < municipios; i++) {
		for (int j = 0; j < SERIE-1; j++) {
			int nasc_a1 = estatisticas[i].nascimentos[j];
			int nasc_a2 = estatisticas[i].nascimentos[j+1];
			taxasCrescimento[i][j] = 100 * (razao(nasc_a2, nasc_a1)-1);
		}
	}
	return taxasCrescimento;
}


/**
 * @brief 	Realiza uma avaliacao comparativa do numero de nascimentos em dois 
 *			anos especificos
 * @details Faz uso da taxa de crescimento entre os dois anos em questao para 
 *			identificar que municipios apresentaram maior e menor valores
 * @param 	estatisticas Vetor de objetos do tipo estrutura Stats 
 *		  	para armazenar as estatisticas de cada municipio
 * @param 	municipios Numero de municipios
 * @param	ano1 Ano base de comparacao
 * @param	ano2 Ano base de comparacao
 */
void avaliarCrescimento(Stats* estatisticas, int municipios, int ano1, int ano2) {
	if (ano1 > ano2) {
		cerr << "Nao e possivel realizar analise comparativa" << endl;
	} else {
		// Calculo da taxa de crescimento (percentual) para cada municipio na serie
		float** taxasCrescimento = calcularCrescimentoSerie(estatisticas, municipios);

		// Determinacao da maior e menor taxa de crescimento para os anos em questao
		float max = taxasCrescimento[0][ano2-1995];
		float min = taxasCrescimento[0][ano2-1995];
		string municipio_max = estatisticas[0].nome;
		string municipio_min = estatisticas[0].nome;
		for (int i = 1; i < municipios; i++) {
			if (taxasCrescimento[i][ano2-1995] > max) {
				max = taxasCrescimento[i][ano2-1995];
				municipio_max = estatisticas[i].nome;
			}
			if (taxasCrescimento[i][ano2-1995] < min) {
				min = taxasCrescimento[i][ano2-1995];
				municipio_min = estatisticas[i].nome;
			}
		}

		// Determinacao de queda ou crescimento
		string sit_min = (min < 0)? "maior taxa de queda" : "menor taxa de crescimento";
		string sit_max = (max < 0)? "menor taxa de queda" : "maior taxa de crescimento";

		// Municipio com maior taxa de queda ou menor taxa de crescimento
		cout << "\n... Municipio com " << sit_min << " ";
		cout << ano1 << "-" << ano2 << ": " << municipio_min;
		cout << " (" << showpos << fixed << setprecision(2) << min << "%)" << endl;

		// Municipio com menor taxa de queda ou maior taxa de crescimento
		cout << "... Municipio com " << sit_max << " " << noshowpos;
		cout << ano1 << "-" << ano2 << ": " << municipio_max;
		cout << " (" << showpos << fixed << setprecision(2) << max << "%)" << endl;

		// Liberacao de memoria dinamicamente alocada
		for (int i = 0; i < municipios; i++) {
			if (taxasCrescimento[i] != NULL) {
				delete[] taxasCrescimento[i];
			}
		}
		if (taxasCrescimento != NULL) {
			delete[] taxasCrescimento;
		}
	}
}


/**
 * @brief 	Realiza leitura de arquivo com municipios alvo
 * @param 	estatisticas Vetor de objetos do tipo de estrutura Stats para armazenar
 *		  	as estatisticas do dataset completo
 * @param 	municipios Numero total de municipios
 * @param 	arq_alvos Stream de entrada de arquivo referente aos municipios alvo
 * @param	numAlvos Numero de municipios alvo
 * @return 	Vetor de objetos do tipo estrutura Stats para armazenar as estatisticas 
 *		   	de cada municipio
 */
Stats* carregarAlvos(Stats* estatisticas, int municipios, ifstream& arq_alvos, int numAlvos) {
	// Vetor de objetos do tipo estrutura Stats para representar as estatisticas
	// dos municipios alvo
	Stats* municipiosAlvo = new Stats[numAlvos];
	if (!municipiosAlvo) {
		cerr << "Erro em alocacao dinamica de memoria" << endl;
		cerr << "O programa sera encerrado" << endl;
		exit(1);
	}

	cout << endl << "... Lendo arquivo input/alvos.dat" << endl;

	int alvo = 0;
	string codigo;
	arq_alvos.clear();
	arq_alvos.seekg(0, arq_alvos.beg);		// Leitura a partir do inicio
	while (arq_alvos >> codigo) {
		for (int i = 0; i < municipios; i++) {
			if (estatisticas[i].codigo.compare(codigo) == 0) {
				municipiosAlvo[alvo] = estatisticas[i];
				alvo++;
			}
		}
	}

	// Impressao dos nomes dos municipios selecionados como alvos
	cout << "......[" << numAlvos << "] municipios definidos como alvo" << endl;
	for (int i = 0; i < numAlvos; i++) {
		cout << "......{ " << municipiosAlvo[i].nome << " }" << endl;
	}

	return municipiosAlvo;
}


/**
 * @brief 	Calcula as taxas de crescimento para um conjunto de municipios para a
 *		  	serie considerada
 * @details	Gera um arquivo de saida contendo as taxas de crescimento de cada municipio
 *			na serie historica
 * @param 	estatisticas Vetor de objetos do tipo estrutura Stats para armazenar as 
 *		  	estatisticas dos municipios em questao
 * @param 	municipios Numero de municipios
 */
void analisarCrescimentoSerie(Stats* estatisticas, int municipios) {
	// Stream de saida em arquivo para taxas de crescimento na serie
	ofstream arq_extra("output/extra.dat");
	verificaArquivo(arq_extra, "output/extra.dat");

	// Calculo da taxa de crescimento (percentual) para cada municipio na serie
	float** taxasCrescimento = calcularCrescimentoSerie(estatisticas, municipios);

	// Gravacao em arquivo de saida
	arq_extra << "Serie" << " ";
	for (int i = 0; i < municipios; i++) {
		arq_extra << "\"" << estatisticas[i].nome << "\" ";
	}
	arq_extra << endl;
	for (int i = 0; i < SERIE-1; i++) {
		arq_extra << "\"" << 1994 + i << "-" << 1994 + i + 1 << "\" ";
		for (int j = 0; j < municipios; j++) {
			arq_extra << taxasCrescimento[j][i] << " ";
		}
		arq_extra << endl;
	}
	cout << "... Arquivo output/extra.dat gerado" << endl;
	arq_extra.close();

	// Liberacao de memoria dinamicamente alocada
	for (int i = 0; i < municipios; i++) {
		if (taxasCrescimento[i] != NULL) {
			delete[] taxasCrescimento[i];
		}
	}
	if (taxasCrescimento != NULL) {
		delete[] taxasCrescimento;
	}
}

